# Secure Jenkins Container

## Development Setup on Amazon Linux

* Install docker: `sudo yum install -y`
* Start docker: `sudo service docker start`

## To build the image

* Build the image `docker build ---tag vkhazin/jenkins:1.0`

## To run a container

* Configure FQDN defined in .env to point to the host publically exposed ip on ports 80 & 443, otherwise the cert request will fail
* Host folder "data/jenkins" must be accesible for user with uuid 1000 - jenkins user in the container
* Review ./.env settings or pass individual environment variables, e.g.: `docker run -e "DOMAINNAME=jenkins.icssolutions.ca"`
* Launch the container: `./run.sh`
